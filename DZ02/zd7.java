import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class zd7 {

    public static void main(String argv[]) {
        Connection connection = otvoriKonekciju();

        try {
            while (1 == 1) {
                Statement stmt = connection.createStatement();
                ResultSet rs = stmt.executeQuery("EXECUTE PROCEDURE studentiPoGrupama()");
                while (rs.next()) {
                    String grupa = rs.getString(1);
                    String jmbag = rs.getString(2);
                    String prezime = rs.getString(3);
                    String ime = rs.getString(4);

                    System.out.println(grupa + " " + jmbag + " " + ime + " " + prezime);
                }
                rs.close();
            }
        } catch (SQLException exception) {
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
        }
    }

    private static Connection otvoriKonekciju() {
        Connection conn = null;
        String url = "jdbc:informix-sqli://sbp.edu:1526/sbpdz2" +
            ":INFORMIXSERVER=sbp;DB_LOCALE=hr_hr.utf8;CLIENT_LOCALE=hr_hr.utf8;" +
            "user=horvat;password=horvat";

        try {
            Class.forName("com.informix.jdbc.IfxDriver");
            System.out.println("Informix JDBC driver je ucitan i registriran.");
        } catch (ClassNotFoundException exception) {
            System.out.println("Pogreška: nije uspjelo ucitavanje Informix JDBC driver-a.");
            System.out.println(exception.getMessage());
            System.exit(-1);
        }

        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Konekcija je uspostavljena.");
        } catch (SQLException exception) {
            System.out.println("Pogreška: nije uspjelo uspostavljanje konekcije.");
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
            System.exit(-1);
        }
        return conn;
    }
}