import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

public class Client {
   public static void main(String argv[]) {
      String url = null;
      Connection conn = null;
      PreparedStatement pstmt = null;
      Calendar calendar;
      url =   "jdbc:informix-sqli://sbp.edu:1526/testBackup" 
                 + ":INFORMIXSERVER=sbp;DB_LOCALE=hr_hr.utf8;CLIENT_LOCALE=hr_hr.utf8;" 
                 + "user=horvat;password=horvat";

      // ucitavanje i registriranje Informix JDBC driver-a
      try {
         Class.forName("com.informix.jdbc.IfxDriver");
         System.out.println("Informix driver ucitan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogreska: nije uspjelo ucitavanje Informix JDBC driver-a.");
         exception.printStackTrace();
         return;
      }
      // uspostavljanje konekcije
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogreska: nije uspjelo stvaranje konekcije.");
         exception.printStackTrace();
         return;
      }

      try {
         conn.setAutoCommit(true);
         pstmt = conn.prepareStatement("INSERT INTO stoJeObavljeno VALUES (?, ?)");
      } catch (SQLException exception) {
         System.out.println("Pogreska: nije uspjelo prevodjenje SQL naredbe.");
         exception.printStackTrace();
         return;
      }
      try {
         // obrisi eventualne stare zapise iz relacije stoJeObavljeno
         Statement statement = conn.createStatement();
         statement.executeUpdate("DELETE FROM stoJeObavljeno");
         System.out.println("Obrisani svi zapisi iz relacije stoJeObavljeno");
         
         // svake sekunde ubaci jedan zapis u relaciju stoJeObavljeno
         int rbr = 0;
         while (true) {
            rbr++;
            pstmt.setInt(1, rbr);
            pstmt.setString(2, "Neki tekst");
            pstmt.executeUpdate();
            calendar = Calendar.getInstance();
            System.out.println("Vrijeme:" +  calendar.getTime() + 
                               ". Upisan rbr " + rbr + ". Committed.");
            try {
               Thread.sleep(1000);
            }
            catch (Exception e) {
               // ne ocekuje se da ce se ovdje dogoditi exception
            }
         }  
      } catch (SQLException exception) {
         System.out.println("Pogreska: pri postavljanju parametara ili izvrsavanju SQL naredbe.");
         System.out.println(exception);
         return;
      }
   }
}
