create table actor (
  id int not null,
  firstName nchar(80) not null,
  lastName nchar(80) not null,
  birthday date not null
);
	
create table series (
  id int not null,
  name nchar(80) not null,
  status nchar(1) not null
);
	
create table series_actors (
  actor_id int not null,
  series_id int not null
);
	