create or replace function fillActors() returns void as $$
  declare
    i int;
    j int;
    id int := 0;
  begin
    for i in 1..100 loop
      for j in 1..10 loop
        id = id + 1;
        insert into actor values (id, 'john ' || i, 'doe ' || j, to_date('01.01.1970', 'DD.MM.YYYY') + j * 7 + i * j);
      end loop;
    end loop;
  end;
$$ language plpgsql;

create or replace function fillSeries() returns void as $$
  declare
    i int;
    id int := 0;
  begin
    for i in 1..20 loop
      id = id + 1;
      insert into series values (id, 'dukes of ' || id || ' ' || i, 'F');
      id = id + 1;
      insert into series values (id, 'dukes of ' || id || ' ' || i, 'A');
      id = id + 1;
      insert into series values (id, 'dukes of ' || id || ' ' || i, 'U');
      id = id + 1;
      insert into series values (id, 'dukes of ' || id || ' ' || i, 'F');
      id = id + 1;
      insert into series values (id, 'dukes of ' || id || ' ' || i, 'A');
    end loop;
  end;
$$ language plpgsql;

create or replace function fillSeriesActors() returns void as $$
  declare
    i int;
    j int;
  begin
    for i in 1..100 loop
      for j in 1..10 loop
        insert into series_actors values (i, j);
      end loop;
    end loop;
    for i in 101..1000 loop
      insert into series_actors values (i, i / 10);
    end loop;
  end;
$$ language plpgsql;

delete from actor;
delete from series;
delete from series_actors;

select fillSeries(), fillActors(), fillSeriesActors();