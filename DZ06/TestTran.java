import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TestTran {
   
   public static void main(String argv[]) {
      Integer sifRacun = new Integer(argv[0]);
      BigDecimal iznos = new BigDecimal(argv[1]);
      
      Connection connection = otvoriKonekciju();
      
      try {
         unosPromet(connection, sifRacun, iznos);
      }
      catch (SQLException exception) {
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
      }
   }
   
   private static void unosPromet(Connection connection
                                , Integer sifRacun
                                , BigDecimal iznos) throws SQLException {

      Statement stmt = connection.createStatement();
    
      // neka svaka SQL naredba NE BUDE transakcija za sebe
      connection.setAutoCommit(false);
      


      try {
         System.out.println("Transakcija je zapo�ela.");
         stmt.executeUpdate("INSERT INTO promet VALUES (0, " + sifRacun + ", " + iznos + ")");
         System.out.println("INSERT obavljen.");
         stmt.executeUpdate("UPDATE racun SET stanje = stanje + " + iznos +
               " WHERE sifRacun = " + sifRacun);
         System.out.println("UPDATE obavljen.");
      }
      catch (SQLException exception) {
         connection.rollback();
         System.out.println("Transakcija je poni�tena.");
         if (exception.getErrorCode() == -530 &&
             exception.getMessage().indexOf("chkracunstanje") != -1) {
            throw new SQLException("Nedopu�ten minus", exception.getSQLState(), -746);
         }
         else {
            throw exception;
         }
      }
      connection.commit();
      System.out.println("Transakcija je potvr�ena.");
   }
   
   private static Connection otvoriKonekciju () {
      Connection conn = null;
      // sastavljanje JDBC URL:
      //                                       ip_adresa:port/baza
      String url = "jdbc:informix-sqli://sbp.edu:1526/testTran" 
                 + ":INFORMIXSERVER=sbp;DB_LOCALE=hr_hr.utf8;CLIENT_LOCALE=hr_hr.utf8;" 
                 + "user=horvat;password=horvat";
      
      // u�itavanje i registriranje Informix JDBC driver-a
      try {
         Class.forName("com.informix.jdbc.IfxDriver");
         System.out.println("Informix JDBC driver je u�itan i registriran.");
      } catch (ClassNotFoundException exception) {
         System.out.println("Pogre�ka: nije uspjelo u�itavanje Informix JDBC driver-a.");
         System.out.println(exception.getMessage());
         System.exit(-1);
      }
      
      // uspostavljanje konekcije
      try {
         conn = DriverManager.getConnection(url);
         System.out.println("Konekcija je uspostavljena.");
      } catch (SQLException exception) {
         System.out.println("Pogre�ka: nije uspjelo uspostavljanje konekcije.");
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
         System.exit(-1);
      }
      return conn;
   }
}
