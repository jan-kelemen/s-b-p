create procedure assignExtra(p_actorId int, p_seriesId int)
	define sqle, isame integer;
	define errdata char(80);

	on exception set sqle, isame, errdata
		rollback work;
		if sqle = -530 and errdata like '%chkseriesextrasneeded%' then
			raise exception -746, 0, 'Nije potrebno jos statista';
		else
			raise exception sqle, isame, errdata;
		end if;
	end exception;
	begin work;
		insert into seriesExtras values (p_actorId, p_seriesId);
		update series set extrasNeeded = extrasNeeded - 1 where id = p_seriesId;
	commit work;	
end procedure;