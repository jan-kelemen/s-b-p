create database lpiis3 with log;

create table actor (
	id int not null,
	name nchar(80) not null,
	primary key (id) constraint pkPerson
);

create table series (
	id int not null,
	name nchar(80) not null,
	extrasNeeded int not null,
	primary key (id) constraint pkSeries,
	check (extrasNeeded >= 0) constraint chkSeriesExtrasNeeded
);

create table seriesExtras (
	actorId int not null,
	seriesId int not null,
	primary key (actorId, seriesId) constraint pkSeriesExtras,
	foreign key (actorId) references actor (id) constraint fkSeriesExtrasActor,
	foreign key (seriesId) references series (id) constraint fkSeriesExtresSeries
);
	
insert into actor values (1, 'Marina Sirtis');
insert into actor values (2, 'Terry Farrell');
insert into actor values (3, 'Nana Visitor');

insert into series values (1, 'Star Trek: The Original Series', 0);
insert into series values (2, 'Star Trek: The Next Generation', 1);
insert into series values (3, 'Star Trek: Deep Space 9', 2);

insert into seriesExtras values (1, 1);