create database lpiis3;

create table actor (
	id int not null,
	name nchar(80) not null,
	constraint pkPerson primary key (id)
);

create table series (
	id int not null,
	name nchar(80) not null,
	extrasNeeded int not null,
	constraint pkSeries primary key (id),
	constraint chkSeriesExtrasNeeded check (extrasNeeded >= 0)
);

create table seriesExtras (
	actorId int not null,
	seriesId int not null,
	constraint pkSeriesExtras primary key (actorId, seriesId),
	constraint fkSeriesExtrasActor foreign key (actorId) references actor (id),
	constraint fkSeriesExtresSeries foreign key (seriesId) references series (id)
);
	
insert into actor values (1, 'Marina Sirtis');
insert into actor values (2, 'Terry Farrell');
insert into actor values (3, 'Nana Visitor');

insert into series values (1, 'Star Trek: The Original Series', 0);
insert into series values (2, 'Star Trek: The Next Generation', 1);
insert into series values (3, 'Star Trek: Deep Space 9', 2);

insert into seriesExtras values (1, 1);