import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class Z3C {

    public static void main(String argv[]) {
        Integer actorId = new Integer(argv[0]);
        Integer seriesId = new Integer(argv[1]);

        Connection connection = openConnection();

        try {
            assignExtra(connection, actorId, seriesId);
        } catch (SQLException exception) {
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
        }
    }

    private static void assignExtra(Connection connection, Integer actorId, Integer seriesId) throws SQLException {
        connection.setAutoCommit(false);

        try {
            System.out.println("Transakcija zapocela");
			PreparedStatement deferred = prepareDeferred(connection);
			deferred.executeUpdate();
            PreparedStatement insert = prepareInsert(connection, actorId, seriesId);
            insert.executeUpdate();
            System.out.println("Insert obavljen");
            PreparedStatement update = prepareUpdate(connection, seriesId);
            update.executeUpdate();
            System.out.println("Update obavljen");
        } catch (SQLException e) {
            connection.rollback();
            if (e.getErrorCode() == -530 && e.getMessage().indexOf("chkseriesextrasneeded") != -1) {
                throw new SQLException("Nije potrebno jos statista", e.getSQLState(), -746);
            } else {
                throw e;
            }
        }
        connection.commit();
        System.out.println("Transakcija potvrdena");
    }

	private static PreparedStatement prepareDeferred(Connection conn) throws SQLException {
		return conn.prepareStatement("set constraints all deferred");
	}
	
    private static PreparedStatement prepareInsert(Connection conn, Integer actorId, Integer seriesId) throws SQLException {
        PreparedStatement s = conn.prepareStatement("insert into seriesExtras values (?, ?)");
        s.setInt(1, actorId);
        s.setInt(2, seriesId);
        return s;
    }

    private static PreparedStatement prepareUpdate(Connection conn, Integer seriesId) throws SQLException {
        PreparedStatement s = conn.prepareStatement("update series set extrasNeeded = extrasNeeded - 1 where id = ?");
        s.setInt(1, seriesId);
        return s;
    }

    private static Connection openConnection() {
        Connection conn = null;
        String url = "jdbc:informix-sqli://sbp.edu:1526/lpiis3" +
            ":INFORMIXSERVER=sbp;DB_LOCALE=hr_hr.utf8;CLIENT_LOCALE=hr_hr.utf8;" +
            "user=horvat;password=horvat";

        try {
            Class.forName("com.informix.jdbc.IfxDriver");
            System.out.println("Informix JDBC driver je ucitan i registriran.");
        } catch (ClassNotFoundException exception) {
            System.out.println("Pogreska: nije uspjelo ucitavanje Informix JDBC driver-a.");
            System.out.println(exception.getMessage());
            System.exit(-1);
        }

        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Konekcija je uspostavljena.");
        } catch (SQLException exception) {
            System.out.println("Pogreska: nije uspjelo uspostavljanje konekcije.");
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
            System.exit(-1);
        }
        return conn;
    }
}