import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Z4C {

    public static void main(String argv[]) {
        Integer actorId = new Integer(argv[0]);
        Integer seriesId = new Integer(argv[1]);

        Connection connection = openConnection();

      try {
         connection.setAutoCommit(false);
         
         Statement stmt = connection.createStatement();
         stmt.executeUpdate("execute procedure assignExtra(" + actorId + ", " + seriesId + ")");
         System.out.println("Transakcija je potvrdena.");
      }
      catch (SQLException exception) {
         System.out.println(exception.getErrorCode() + " " + exception.getMessage());
      }
    }

    private static Connection openConnection() {
        Connection conn = null;
        String url = "jdbc:informix-sqli://sbp.edu:1526/lpiis3" +
            ":INFORMIXSERVER=sbp;DB_LOCALE=hr_hr.utf8;CLIENT_LOCALE=hr_hr.utf8;" +
            "user=horvat;password=horvat";

        try {
            Class.forName("com.informix.jdbc.IfxDriver");
            System.out.println("Informix JDBC driver je ucitan i registriran.");
        } catch (ClassNotFoundException exception) {
            System.out.println("Pogreska: nije uspjelo ucitavanje Informix JDBC driver-a.");
            System.out.println(exception.getMessage());
            System.exit(-1);
        }

        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Konekcija je uspostavljena.");
        } catch (SQLException exception) {
            System.out.println("Pogreska: nije uspjelo uspostavljanje konekcije.");
            System.out.println(exception.getErrorCode() + " " + exception.getMessage());
            System.exit(-1);
        }
        return conn;
    }
}